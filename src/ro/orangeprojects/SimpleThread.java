package ro.orangeprojects;

public class SimpleThread extends Thread {

    public SimpleThread(String hotel) {

        super(hotel); // am chemat un constructor al parintelui, in cazul nostru Thread.

    }

    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Loop " + i + ": " + getName());
            try {
                sleep((int) (Math.random() * 1000));
            } catch (InterruptedException e) {
            }


        }
        System.out.println("DONE " + getName());
    }
}